String.prototype.capitalize = function() {
    return this.charAt(0).toUpperCase() + this.slice(1);
}

var generator = {
  cimszavak: `erő, egészség, munka, szabadság, egyenlőség, testvériség, kitartás, büszkeség, jólét, becsület, föld, haza, egyetértés, küzdelem, Isten, család, változás, tisztesség, igazság, igazságosság, nemzet, haladás, fejlődés, rezsicsökkentés, tánc, béremelés, egészségügy, oktatás, polgárság, jövedelem, olimpia, rend, köztársaság, bizalom, létbiztonság, közbiztonság, növekedés, jogbiztonság, törvényesség, dicsőség, biztonság, cselekvés, jövő, felelősség, tudás, remény, elszántság, bátorság, hit, lendület, tisztánlátás, szakértelem, függetlenség, békesség, megújulás, őszinteség, erkölcsösség, emberiesség, hagyomány, hitelesség, párbeszéd, összefogás, egység, demokrácia, siker, megoldás`.split(", "),

  utotag: "párt, pártja, szövetség, szövetsége, koalíció, koalíciója, mozgalom".split(", "),
  negativumok: "röghözkötés, migráns, félelem, határzár, megszorítás, megfélemlítés, csőd, adóemelés, hanyatlás, sínylődés, nyomor, pusztulás, rabiga, nemzethalál, élősködés, bizonytalanság, kizsákmányolás, szégyen, viszály, reménytelenség, hazugság, hiteltelenség, felelőtlenség, függés, gazemberség, elnyomás, erkölcstelenség, szélsőség, gyűlöletkeltés, mocsok, munkanélküliség, szegénység, diktatúra, megosztottság, korrupció, bűnözés, kultúrharc, telhetetlenség, maffia, mutyi, hablaty, provokáció".split(", "),
  osszekapcsolasok : [
    "# helyett *!",
    "több *, kevesebb #!",
    "inkább *, mint #!",
    "* vagy #?!",
    "*, * és *"
  ],

  utotagok : "párt, pártja, szövetsége, szövetség, koalíció, koalíciója, egyesület, egyesülete, mozgalom, mozgalma".split(", "),

  randomIndex : function (a){
    return Math.floor(Math.random() * a.length)
  },

  randomElem : function (a){
    return a[this.randomIndex(a)]
  },
  akro : function(n){
    var kezdobetu = function(sz){
      let ketjegyuek = ["cs", "dz", "gy",  "ly", "ny", "sz", "ty", "zs"]
      let i = ketjegyuek.indexOf(sz.substr(0, 2))
      if(i > -1)
        return ketjegyuek[i]
      else
        return sz[0]
    }
    return n.split(" ").reduce((acc, elem) => acc + kezdobetu(elem.toLowerCase()).capitalize(), "")
  },
  ujSzlogen : function(){
    var r = this.randomElem(this.osszekapcsolasok)

    var keresztindex = r.indexOf("#")
    while(keresztindex != -1){
      r = r.replace("#", this.randomElem(this.negativumok))
      keresztindex = r.indexOf("#")
    }

    var csillagindex = r.indexOf("*")
    while(csillagindex != -1){
      r = r.replace("*", this.randomElem(this.cimszavak))
      csillagindex = r.indexOf("*")
    }

    return r.capitalize()
  },

  ujPartnev : function(){
    return this.randomElem(this.cimszavak).capitalize() + " " + this.randomElem(this.utotagok).capitalize()
  },

  ujPart : function(){
    var p = this.ujPartnev()
    return {
      partnev : p,
      akro : this.akro(p),
      szlogen : this.ujSzlogen(),
      toString : function(){
        return this.akro + " " + this.partnev + ' - "' + this.szlogen + '"'
      }
    }
  },
  ujPartElement : function(){
    var p = this.ujPart()
    var checkbox = $("<div>",
      {
        class : "checkcircle",
        click : function(e){
          $("#szavazo").animate({top: (window.innerHeight - 100) + "px"}, 500)
          $(".checked").removeClass("checked")
          if($(this).hasClass("checked"))
            $(this).removeClass("checked")
          else
            $(this).addClass("checked")
        }
      }
    )
    var akro = $("<div>",
      {
        class : "akro",
        html : p.akro
      }
    )
    var partnev = $("<div>",
      {
        class : "partnev",
        html : p.partnev
      }
    )
    var szlogen = $("<div>",
      {
        class : "szlogen",
        html : p.szlogen
      }
    )
    return $(
      "<div>",
      {
        class : "part",
        html : [checkbox, akro, partnev, szlogen]
      }
    )
  }
}


$("#szavazok").click(function(e){
  $("#koszi").animate({top : 0}, 500)
  let valasztott = $(".checked").parent().children()[2].innerHTML
  let sz = window.localStorage.getItem("szavazatok")
  if(sz)
    sz = JSON.parse(sz)
  else
    sz = []

  let i = sz.findIndex((p)=>p.nev == valasztott)
  if(i > -1)
    sz[i].szavazatok++;
  else
    sz.push({nev : valasztott, szavazatok : 1})

  window.localStorage.setItem("szavazatok", JSON.stringify(sz))
  window.setTimeout(()=>window.location.reload(), 8000)
}
)

for(let i = 0; i< 100; i++)
  $("#partlista").append(generator.ujPartElement())
let currentHeight = $("#partlista").height()

$(window).scroll(function(e){
  console.log($("html").scrollTop())
  if($("html").scrollTop() > currentHeight - window.innerHeight - 400){
    for(let i = 0; i< 3; i++)
      $("#partlista").append(generator.ujPartElement())
    currentHeight = $("#partlista").height()
  }
})
